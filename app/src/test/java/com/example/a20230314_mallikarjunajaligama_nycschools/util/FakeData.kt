package com.example.a20230314_mallikarjunajaligama_nycschools.util

import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.SchoolDetailsApiModel
import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.SchoolDetailsApiModelItem
import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.Schools
import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.SchoolsItem

val fakeApiSchoolDetails = SchoolDetailsApiModel().apply {
    add(
        SchoolDetailsApiModelItem(
            dbn = "dbn",
            num_of_sat_test_takers = "totalNumberOfTestTakers",
            sat_critical_reading_avg_score = "averageReadingScore",
            sat_math_avg_score = "averageMathScore",
            sat_writing_avg_score = "averageWritingScore",
            school_name = "schoolName"
        )
    )
}



val fakeApiSchoolsList: Schools = Schools().apply {
    add(
        SchoolsItem(
            dbn = "dbn",
            school_name = "schoolName",
            overview_paragraph = "overviewParagraph",
            phone_number = "phoneNumber",
            school_email = "schoolEmail",
            state_code = "stateCode",
            total_students = "totalStudents",
            transfer = "transfer",
            website = "website",
            zip = "zip"
        )
    )
}
