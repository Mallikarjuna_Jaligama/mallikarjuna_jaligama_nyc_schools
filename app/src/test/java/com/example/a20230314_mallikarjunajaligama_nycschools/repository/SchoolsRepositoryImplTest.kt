package com.example.a20230314_mallikarjunajaligama_nycschools.repository

import com.example.a20230314_mallikarjunajaligama_nycschools.repository.network.SchoolsApiService
import com.example.a20230314_mallikarjunajaligama_nycschools.util.fakeApiSchoolDetails
import com.example.a20230314_mallikarjunajaligama_nycschools.util.fakeApiSchoolsList
import com.example.schools.util.*
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class SchoolsRepositoryImplTest {


    class TestObjects {
        val mockSchoolsApiService: SchoolsApiService = mockk()
        val subject: SchoolsRepositoryImpl = SchoolsRepositoryImpl(mockSchoolsApiService)
    }

    @Test
    fun `getSchoolsList returns list of DomainSchoolItem if apiService is successful`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolsList() } returns fakeApiSchoolsList

        // Act
        val result = testObjects.subject.getSchoolsList()

        // Assert
        assertEquals(fakeApiSchoolsList, result)
    }


    @Test
    fun `getSchoolDetails returns DomainSchoolDetails if apiService is successful`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolDetails("dbn") } returns fakeApiSchoolDetails


        // Act
        val result = testObjects.subject.getSchoolDetails("dbn")

        // Assert
        assertEquals(fakeApiSchoolDetails.first(), result)
    }


}
