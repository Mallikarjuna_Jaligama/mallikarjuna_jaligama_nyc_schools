package com.example.a20230314_mallikarjunajaligama_nycschools.di

import com.example.a20230314_mallikarjunajaligama_nycschools.repository.network.SchoolsApiConstants
import com.example.a20230314_mallikarjunajaligama_nycschools.repository.network.SchoolsApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun providesNetworkService(retrofit: Retrofit.Builder): SchoolsApiService {
        return retrofit.baseUrl(SchoolsApiConstants.BASE_URL)
            .build()
            .create(SchoolsApiService::class.java)
    }
}