package com.example.a20230314_mallikarjunajaligama_nycschools.di

import com.example.a20230314_mallikarjunajaligama_nycschools.repository.SchoolsRepository
import com.example.a20230314_mallikarjunajaligama_nycschools.repository.SchoolsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FeatureModule {

    @Binds
    abstract fun bindSchoolsRepository(
        schoolsRepositoryImpl: SchoolsRepositoryImpl
    ): SchoolsRepository
}