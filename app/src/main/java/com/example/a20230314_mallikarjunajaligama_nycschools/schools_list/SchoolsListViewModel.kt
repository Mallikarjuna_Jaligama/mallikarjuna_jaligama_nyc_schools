package com.example.a20230314_mallikarjunajaligama_nycschools.schools_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.Schools
import com.example.a20230314_mallikarjunajaligama_nycschools.repository.SchoolsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolsListViewModel @Inject constructor(
    private val schoolsRepository: SchoolsRepository
) : ViewModel() {

    private val _schoolsList = MutableStateFlow<SchoolsListState>(SchoolsListState.Empty())
    val schoolsList: LiveData<SchoolsListState>
        get() = _schoolsList.asLiveData()

    init {
        initializeSchoolsScreen()
    }

    private fun initializeSchoolsScreen() {
        viewModelScope.launch {
            _schoolsList.value = SchoolsListState.Loading()
            try{
                val result = schoolsRepository.getSchoolsList()
                onSuccess(result)
            }catch (e: Exception){
                _schoolsList.value =
                    SchoolsListState.Failure("Fetching schools failed! error: $e")
            }
        }
    }

    private fun onSuccess(response: Schools) {
        if (response.isEmpty()) _schoolsList.value = SchoolsListState.Empty()
        else _schoolsList.value = SchoolsListState.Success(response)
    }
}

sealed class SchoolsListState() {
    class Empty : SchoolsListState()
    class Loading : SchoolsListState()
    data class Success(val list: Schools) : SchoolsListState()
    data class Failure(val message: String) : SchoolsListState()
}