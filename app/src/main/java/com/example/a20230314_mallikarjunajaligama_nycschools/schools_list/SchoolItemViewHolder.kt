package com.example.a20230314_mallikarjunajaligama_nycschools.schools_list

import androidx.recyclerview.widget.RecyclerView
import com.example.a20230314_mallikarjunajaligama_nycschools.databinding.SchoolListItemBinding
import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.SchoolsItem


class SchoolItemViewHolder(
    private val binding: SchoolListItemBinding,
    private val itemClickListener: SchoolItemAdapter.ItemClickListener
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(school: SchoolsItem) = binding.run {
        this.school = school
        this.listener = itemClickListener
        executePendingBindings()
    }
}