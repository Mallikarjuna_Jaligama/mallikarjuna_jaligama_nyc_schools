package com.example.a20230314_mallikarjunajaligama_nycschools.schools_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230314_mallikarjunajaligama_nycschools.R
import com.example.a20230314_mallikarjunajaligama_nycschools.databinding.SchoolListItemBinding
import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.Schools

class SchoolItemAdapter(private val items: Schools, private val itemClickListener: ItemClickListener) : RecyclerView.Adapter<SchoolItemViewHolder>() {

  interface ItemClickListener {
    fun onClickItem(schoolId: String)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolItemViewHolder =
    DataBindingUtil.inflate<SchoolListItemBinding>(LayoutInflater.from(parent.context), R.layout.school_list_item, parent, false).run {
      SchoolItemViewHolder(this, itemClickListener)
    }

  override fun getItemCount(): Int = items.size

  override fun onBindViewHolder(viewHolder: SchoolItemViewHolder, position: Int) {
    viewHolder.bind(items[position])
  }
}