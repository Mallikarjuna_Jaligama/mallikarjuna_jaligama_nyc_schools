package com.example.schools.util

import java.io.Serializable

sealed class ApiResult<out T> {
    data class Success<T>(val value: T) : ApiResult<T>()
    data class Failure(val error: Serializable) : ApiResult<Nothing>()
}