package com.example.a20230314_mallikarjunajaligama_nycschools.repository.network

object SchoolsApiConstants {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
}