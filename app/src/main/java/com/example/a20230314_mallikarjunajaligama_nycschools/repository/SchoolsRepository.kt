package com.example.a20230314_mallikarjunajaligama_nycschools.repository

import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.SchoolDetailsApiModelItem
import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.Schools
import com.example.a20230314_mallikarjunajaligama_nycschools.repository.network.SchoolsApiService
import javax.inject.Inject

interface SchoolsRepository {
    suspend fun getSchoolsList(): Schools
    suspend fun getSchoolDetails(id: String): SchoolDetailsApiModelItem
}

class SchoolsRepositoryImpl @Inject constructor(
    private val schoolsApiService: SchoolsApiService
) : SchoolsRepository {

    override suspend fun getSchoolsList(): Schools {
        return schoolsApiService.getSchoolsList()
    }

    override suspend fun getSchoolDetails(id: String): SchoolDetailsApiModelItem {
     return schoolsApiService.getSchoolDetails(id).first()
    }
}