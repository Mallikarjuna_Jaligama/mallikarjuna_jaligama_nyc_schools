package com.example.a20230314_mallikarjunajaligama_nycschools.school_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.a20230314_mallikarjunajaligama_nycschools.models.api_models.SchoolDetailsApiModelItem
import com.example.a20230314_mallikarjunajaligama_nycschools.repository.SchoolsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolTestDetailsViewModel @Inject constructor(
    private val schoolsRepository: SchoolsRepository
) : ViewModel() {

    private val _schoolDetails = MutableStateFlow<SchoolDetailsState>(SchoolDetailsState.Loading())
    val schoolDetails: LiveData<SchoolDetailsState>
        get() = _schoolDetails.asLiveData()

    fun getSchoolDetails(id: String) {
        viewModelScope.launch {
            _schoolDetails.value = SchoolDetailsState.Loading()
            try{
               val result = schoolsRepository.getSchoolDetails(id)
                _schoolDetails.value = SchoolDetailsState.Success(result)
            }catch (e:Exception){
                _schoolDetails.value =
                    SchoolDetailsState.Failure("Failed to load school details! error: $e")
            }
        }
    }


}

sealed class SchoolDetailsState() {
    class Loading : SchoolDetailsState()
    data class Success(val schoolDetails: SchoolDetailsApiModelItem) : SchoolDetailsState()
    data class Failure(val message: String) : SchoolDetailsState()
}