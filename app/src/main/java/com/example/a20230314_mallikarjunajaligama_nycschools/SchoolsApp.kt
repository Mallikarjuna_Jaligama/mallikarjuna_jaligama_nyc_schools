package com.example.a20230314_mallikarjunajaligama_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolsApp : Application()